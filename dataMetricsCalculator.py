import numpy as np
import pandas as pd

class dataMetricsCalculator:
    """
    This class calculates metrics on data which are used in the performance of backtests
    """
    def __init__(self):
        pass

    def percentualReturn(self, data, startingValue):
        """Calculate the percentual return of an asset
        Arguments:
            data: {list} value of the asset over time
            starting value: {float} starting value of the asset
        Return {list} percentual change of asset value over time
        """

        df = pd.DataFrame(data=data, columns=["Return"])
        df["Return"] = (df["Return"]/startingValue-1)*100
        pctChange = df["Return"].values.tolist()
        return pctChange

    def drawdown(self, cumulativeReturn):
        """ Calculate the drawdown of a backtested portfolio.
        
        Arguments:
            cumulativeReturn {list} -- cumulative return
            historicalDataFrame {dataframe} -- containing ohlc and timestamp
        
        Returns:
            drawdown {list} -- drawdown of the portfolio in %
        """
        # prepare dataframes, convert timestamp to datetime (relevant for MDD)
        df = pd.DataFrame(data=cumulativeReturn, columns=['cumulativeReturn'])
        df['cumulativeMaxReturn'] = df['cumulativeReturn'].cummax()
        # calculate the drawdown curve and maxDrawdown 
        df['drawdown'] = df['cumulativeReturn'] - df['cumulativeMaxReturn'] 

        drawdown = df['drawdown'].values.tolist()

        return drawdown

    def maxDrawdown(self, drawdown):
        """
        Calculates the maximum drawdown in % from the drawdown data

        Arguments:
            drawdown {list} -- drawdown of the portfolio in %
        Returns:
            maxDrawdown {float} -- maximum drawdown of the portfolio in %
        """
        return min(drawdown) # lowest value is the largest drawdown


    def maxDrawdownDuration(self, cumulativeReturn, historicalDataFrame):
        """
        Calculates the maximum drawdown duration

        :param cumulativeReturn: {list} -- cumulative return of the portfolio
        :param historicalDataFrame: {dataframe} -- ohlc data, used to retrieve timestamps
        :return:
            maxDrawdownDuration {int} -- max drawdown duration, rounded to days
        """

        df = pd.DataFrame(data=cumulativeReturn, columns=['cumulativeReturn'])
        df.loc[:,'date'] = historicalDataFrame.index.values
        df.loc[:,'cumulativeMaxReturn'] = df.loc[:,'cumulativeReturn'].cummax()

        # calculate maxDrawdownDuration
        df.loc[:,'cumulativeMaxReturnShifted'] = df.loc[:,'cumulativeMaxReturn'].shift(periods=1)
        df.loc[df.loc[:,'cumulativeMaxReturn'] > df.loc[:,'cumulativeMaxReturnShifted'], 'newHigh'] = True

        # make new dataframe which excludes the rows without a newHigh
        df2 = df[df.loc[:,'newHigh'].notna()].copy()

        # create a shifted column for the timeDelta calculation (index-index[-1])
        df2.loc[:,'dateShifted'] = df2.loc[:,'date'].shift(periods=1)
        df2.loc[:,'timeDelta'] = df2.loc[:,'date'] - df2.loc[:,'dateShifted']
        maxDrawdownDuration = df2.loc[:,'timeDelta'].max()

        return maxDrawdownDuration.days

    def indexOfOccurredTrades(self, signalData):
        """"
        Calculates the indices of trades occured

        Arguments:
            signalData {list} - trading signals that occured

        Returns:
            indices of occured trade {list}
        """
        return [i for i,value in enumerate(signalData) if value != 0]

    def numberOfTrades(self, signalData):
        """"
        Calculates the amount of trades that occured

        Arguments:
            signalData {list} - trading signals that occured

        Returns:
            Amount of trades {int}
        """
        return len(self.indexOfOccurredTrades(signalData))

    def returnPerTrade(self, signalData, cumulativeReturn):
        """"
        Calculates the return per trade

        Arguments:
            signalData {list} - trading signals that occured during the trading period
            cumulativeReturn {list} - list of cumulative returns during the trading period

        Returns:
            Return per Trade {list}
        """
        indexOfOccuredTrades = self.indexOfOccurredTrades(signalData)
        numberOfTrades = self.numberOfTrades(signalData)
        returnPerTrade = []
        i = 0

        while i<numberOfTrades-1:
            startOfTrade = indexOfOccuredTrades[i]
            endOfTrade = indexOfOccuredTrades[i+1]
            returnPerTrade.append(cumulativeReturn[endOfTrade]-cumulativeReturn[startOfTrade])
            i=i+1

        # append last trade return until now
        startOfLastTrade = indexOfOccuredTrades[-1]
        returnPerTrade.append(cumulativeReturn[-1]-cumulativeReturn[startOfLastTrade])

        return returnPerTrade

    def numberOfWinningTrades(self, signalData, cumulativeReturn):
        """"
        Calculates the number of winning trades occured

        Arguments:
            signalData {list} - trading signals that occured
            cumulativeReturn {list} - list of cumulative returns during the trading period

        Returns:
            Average return on winning trades {float}
        """
        trades = np.array(self.returnPerTrade(signalData, cumulativeReturn))
        return len(trades[trades >=0])

    def numberOfLosingTrades(self, signalData, cumulativeReturn):
        """"
      Calculates the number of losing trades occured

      Arguments:
          signalData {list} - trading signals that occured
          cumulativeReturn {list} - list of cumulative returns during the trading period

      Returns:
          Average return on losing trades {float}
      """
        trades = np.array(self.returnPerTrade(signalData, cumulativeReturn))
        return len(trades[trades <0])

    def winRatio(self, signalData, cumulativeReturn):
        """"
          Calculates the ratio of winning trades vs total trades

          Arguments:
              signalData {list} - trading signals that occured
              cumulativeReturn {list} - list of cumulative returns during the trading period

          Returns:
              Winning trades / total trades {float}
        """
        numberOfWinningTrades = self.numberOfWinningTrades(signalData, cumulativeReturn)
        numberOfTrades = self.numberOfTrades(signalData)
        return numberOfWinningTrades / numberOfTrades

    def loseRatio(self, signalData, cumulativeReturn):
        """"
        Calculates the ratio of losing trades vs total trades

        Arguments:
              signalData {list} - trading signals that occured
              cumulativeReturn {list} - list of cumulative returns during the trading period

        Returns:
              Losing trades / total trades {float}
        """
        numberOfLosingTrades = self.numberOfLosingTrades(signalData, cumulativeReturn)
        numberOfTrades = self.numberOfTrades(signalData)
        return numberOfLosingTrades / numberOfTrades

    def averageReturnPerTrade(self, signalData, cumulativeReturn):
        """"
        Calculates the average return per trade

        Arguments:
            signalData {list} - trading signals that occured during the trading period
            cumulativeReturn {list} - list of cumulative returns during the trading period

        Returns:
            Return per Trade {float}
        """
        returnPerTrade = self.returnPerTrade(signalData, cumulativeReturn)
        numberOfTrades = self.numberOfTrades(signalData)
        return sum(returnPerTrade)/numberOfTrades

    def averageReturnOnWinningTrade(self, signalData, cumulativeReturn):
        """"
       Calculates the average return per winning trade

       Arguments:
           signalData {list} - trading signals that occured during the trading period
           cumulativeReturn {list} - list of cumulative returns during the trading period

       Returns:
           Return per winning trade {float}
       """
        trades = np.array(self.returnPerTrade(signalData, cumulativeReturn))
        return sum(trades[trades >= 0])/len(trades[trades >=0])

    def averageReturnOnLosingTrade(self, signalData, cumulativeReturn):
        """"
       Calculates the average return per losing trade

       Arguments:
           signalData {list} - trading signals that occured during the trading period
           cumulativeReturn {list} - list of cumulative returns during the trading period

       Returns:
           Return per losing trade {float}
       """
        trades = np.array(self.returnPerTrade(signalData, cumulativeReturn))
        return sum(trades[trades < 0]) / len(trades[trades < 0])

    def tradeExpectancy(self, signalData, cumulativeReturn):
        """"
        Calculates the expected return per trade

        Arguments:
            signalData {list} - trading signals that occured during the trading period
            cumulativeReturn {list} - list of cumulative returns during the trading period

        Returns:
            Expected return per trade {float}
        """
        winRatio = self.winRatio(signalData, cumulativeReturn)
        loseRatio = self.loseRatio(signalData, cumulativeReturn)
        averageReturnOnWinningTrade = self.averageReturnOnWinningTrade(signalData, cumulativeReturn)
        averageReturnOnLosingTrade  = self.averageReturnOnLosingTrade(signalData, cumulativeReturn)

        return averageReturnOnWinningTrade*winRatio-averageReturnOnLosingTrade*loseRatio

    def annualizedSharpRatio(self, portfolioValueHistory, interval):
        """Calculating the sharp ratio from the strategy returns, divided by 
        the standard deviation over that strategies return. Risk free rate for 
        now assumed = 0%. 
        Source: https://www.quantstart.com/articles/Sharpe-Ratio-for-Algorithmic-Trading-Performance-Measurement/
        
        Arguments:
            portfolioValueHistory {list} -- history of absolute portfolio value
            interval {str} -- testingInterval of the backtest
        
        Returns:
            sharpRatio {float} -- annualized sharp ratio of the backtest
        """
        df = pd.DataFrame(data=portfolioValueHistory, columns=['portfolioValueHistory'])
        intervalReturn = df['portfolioValueHistory'].shift(periods=-1).pct_change()*100
        riskFreeRate = 0 #assumed
        deltaReturn = intervalReturn - riskFreeRate
        stdReturn = deltaReturn.std()
        amountOfAnnualTradingPeriods = self.amountOfAnnualTradingPeriods(interval)
        sharpRatio = np.sqrt(amountOfAnnualTradingPeriods)*(np.mean(deltaReturn) / stdReturn)
        return sharpRatio

    def amountOfAnnualTradingPeriods(self, interval):
        """Helper function, determines the amount of annual trading periods 
        based on the trading interval. Used in calculating the sharp ratio.
        
        Arguments:
            interval {str} -- trading interval of the backtest
        
        Returns:
            amountOfAnnualTradingPeriods {float} -- amount of klines per year
        """
        annualTradingDays = 365
        if interval == '1m':
            amountOfAnnualTradingPeriods = annualTradingDays * 24 * 60
        elif interval == '15m':
            amountOfAnnualTradingPeriods = annualTradingDays*24*4
        elif interval == '30m':
            amountOfAnnualTradingPeriods = annualTradingDays*24*2
        elif interval == '1h':
            amountOfAnnualTradingPeriods = annualTradingDays*24
        elif interval == '2h':
            amountOfAnnualTradingPeriods = annualTradingDays*12
        elif interval == '4h':
            amountOfAnnualTradingPeriods = annualTradingDays*6
        elif interval == '1d':
            amountOfAnnualTradingPeriods = annualTradingDays
        elif interval == '1w':
            amountOfAnnualTradingPeriods = annualTradingDays/7   
        else:
            print(
                'resample rate of ', interval,
                'is not recognized in the helper function amountOfAnnualTradingPeriods'
                )
        return amountOfAnnualTradingPeriods

    def compoundAnnualGrowthRate(self, historicalDataFrame, portfolioValueHistory): 
        """Calculates the compound annual return of a backtested strategy, 
        independent of the duration of the backtest. Downside of this metric
        is that it does not take the volatility into account. 
        Source: https://www.investopedia.com/terms/c/cagr.asp
        
        Arguments:
            historicalDataFrame {dataframe} -- dataframe with OHLC info
            portfolioValueHistory {list} -- absolute portfolio value
        
        Returns:
            compoundAnnualGrowthRate {float}
        """
        historicalDataFrame.index = historicalDataFrame.index.values
        beginningBalance = portfolioValueHistory[0]
        endingBalance = portfolioValueHistory[-1]
        timeDeltaBacktest = historicalDataFrame.index[-1] - historicalDataFrame.index[0]
        secondsInBacktest = timeDeltaBacktest.total_seconds()
        numberOfYears = secondsInBacktest/(60*60*24*365.25)
        print(endingBalance, beginningBalance, numberOfYears)
        return ((endingBalance / beginningBalance) ** ((1 / numberOfYears)) - 1 ) * 100

