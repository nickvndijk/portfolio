import requests
from selenium import webdriver
import time
from bs4 import BeautifulSoup
import pandas as pd
import os
from datetime import date
from datetime import datetime

class Scraper:
    """"
    This class scrapes content of 'https://automated.isystems.com/'
    """
    def __init__(self, todaysDir, scrapingDir):
        """
        Initializes the webscraper
        Arguments:
            todaysDir: the directory name of todays results
            scrapingDir: the directory name of the summarized scraping data
        """
        # define url
        self.url = 'https://automated.isystems.com/'
        self.nameIdDict = {}
        self.dirToStoreData = todaysDir
        self.alreadyScrapedToday = False
        self.scrapedTableFileName = 'scrapingTable.xlsx'
        self.specialTableFileName = 'special.xlsx'
        self.scrapingDataDir = scrapingDir

        if not os.path.exists(self.dirToStoreData):
            os.mkdir(self.dirToStoreData)
            os.mkdir(os.path.join(self.dirToStoreData, self.scrapingDataDir))
        elif os.path.exists(self.dirToStoreData):
            self.alreadyScrapedToday = True

    def getTableContentsAsText(self):
        """"
        Get contect from website table as text

        Returns:
            beautifulsoup file with html lxml text
        """
        # Open the url with selenium and get all page contents, close browser when done
        try:
            driver = webdriver.Chrome()
            driver.get(self.url)
            time.sleep(3)
            driver.find_element_by_xpath("//select[@name='tableSystems_length']/option[text()='All']").click()
            time.sleep(6)
            driver.find_element_by_id('btnColumn').click()
            time.sleep(1)
            hasClientElement = driver.find_element_by_id('cbColumn7')
            trackedSinceElement = driver.find_element_by_id('cbColumn9')
            suggestedCapitalElement = driver.find_element_by_id('cbColumn28')
            sterlingRatioElement = driver.find_element_by_id('cbColumn34')
            sharpeRatioElement = driver.find_element_by_id('cbColumn32')

            if sharpeRatioElement.get_attribute('checked'):
                pass
            else:
                sharpeRatioElement.click()

            if hasClientElement.get_attribute('checked'):
                pass
            else:
                hasClientElement.click()

            if trackedSinceElement.get_attribute('checked'):
                pass
            else:
                trackedSinceElement.click()

            if suggestedCapitalElement.get_attribute('checked'):
                pass
            else:
                suggestedCapitalElement.click()

            if sterlingRatioElement.get_attribute('checked'):
                pass
            else:
                sterlingRatioElement.click()


            time.sleep(3)
            driver.find_element_by_id('btnColumn').click()
            time.sleep(1)
            text = driver.page_source
        except Exception as e:
            raise e
        finally:
            driver.close()

        # get page source as text
        soup = BeautifulSoup(text, "lxml")

        return soup

    def storeTableInDataFrame(self, soup):
        """
        Stores the data from the table in a dataframe and creates an excel file

        Arguments:
            soup - holds lxml content of scraped table

        Returns:
             Dataframe with table contents
             Specialnames list for names that have special characters
        """
        # define dataframe to store data in
        df = pd.DataFrame(columns=['Name', 'ID', 'Rating', 'Has Clients', 'Suggested Capital',
                                   'Tracked Since', 'Sterling', 'ROI', 'Sharpe Ratio'])
        specialNames = []
        for tr in soup.find_all('tr', )[1:]:
            tds = tr.find_all('td')

            # find all entries with an apostroph and replace these values
            if "'" in tds[1].text:
                name = tds[1].text
                name = name.replace("'", "")
                indexToInsert = name.rfind(" ")
                name = name[:indexToInsert + 1] + '20' + name[indexToInsert + 1:]
                name = name.replace(" ", "%")
                specialNames.append(name)
            else:
                name = tds[1].text

            df = df.append({'Name': name, 'ID': tr.get('data-idsystem'), 'Rating': tds[3].img.get('data-rating'),
                            'Has Clients': tds[6].span.get('data-value'), 'Suggested Capital': tds[14].text,
                            'Tracked Since': tds[8].text, 'Sterling': tds[17].text, 'ROI': tds[12].text,
                            'Sharpe Ratio': tds[16].text}, ignore_index=True)

        df.loc[:, 'Name'] = df['Name'].replace(" ", "%", regex=True)


        filePath = os.path.join(self.dirToStoreData, self.scrapingDataDir, self.scrapedTableFileName)
        df.to_excel(filePath)

        specialFilePath = os.path.join(self.dirToStoreData, self.scrapingDataDir, self.specialTableFileName)
        dfSpecial = pd.DataFrame(specialNames, columns=['Name'])
        dfSpecial.to_excel(specialFilePath)

        return df, specialNames

    def readTableDataFromExcel(self):
        """
        Read table from excel for offline use

        Returns:
            Dataframe with table contents
            Specialnames list for names that have special characters

        """
        df = pd.read_excel(os.path.join(self.dirToStoreData, self.scrapingDataDir, self.scrapedTableFileName))
        dfSpecial = pd.read_excel(os.path.join(self.dirToStoreData, self.scrapingDataDir, self.scrapedTableFileName))

        return df, dfSpecial['Name'].values

    def getEntriesWithClients(self, systemDataFrame):
        """
        Filter dataframe on rows that have clients
        Arguments:
            systemDataFrame {dataframe} - Dataframe with scraped table contents
        """
        df = systemDataFrame[systemDataFrame['Has Clients'] == 'Y']
        df.reset_index(drop=True, inplace=True)
        self.systemsWithClientsDataFrame = df

    def downloadAndStoreExcelFiles(self, systemDataFrame, specialNames):
        """"
        Retrieves names of filtered table and downloads corresponding data sheets

        Arguments:
            systemDataFrame {dataframe} - Dataframe with scraped table contents
            Specialnames {list} - names that have special characters
        """
        baseLink = "https://automated.isystems.com/System/SessionLogExcel/"
        fill = "?strategyName="
        extension = "&header=1&login=1"

        if not self.alreadyScrapedToday:
            for i in range(0, len(systemDataFrame)):
                id =  systemDataFrame.loc[i, 'ID']
                name = systemDataFrame.loc[i, 'Name']
                if systemDataFrame.loc[i, 'Name'] in specialNames:
                    downloadLink = baseLink + id + fill + name
                else:
                    downloadLink = baseLink + id + fill + name + extension

                try:
                    response = requests.get(downloadLink)
                except Exception as e:
                    print('link ' + systemDataFrame.loc[i, 'Name'] + ' not downloaded')

                # Write excel files to new directory
                name = name.replace('"', '' )
                name = name.replace("%", " ")
                fileName = name + '_' + id + '.xls'

                # Store file name and id combination
                self.nameIdDict[fileName] = id

                filePathName = os.path.join(self.dirToStoreData, name + '_' + id + '.xls')
                with open(filePathName, 'wb') as output:
                    output.write(response.content)
        else:
            for i in range(0, len(systemDataFrame)):
                # if files already have been downloaded, only store the dictionary of these files
                id = str(systemDataFrame.loc[i, 'ID'])
                name = systemDataFrame.loc[i, 'Name']

                # Write excel files to new directory
                name = name.replace('"', '')
                name = name.replace("%", " ")
                fileName = name + '_' + id + '.xls'

                # Store file name and id combination
                self.nameIdDict[fileName] = id

if __name__ == "__main__":
    theScraper = Scraper()

    soup = theScraper.getTableContentsAsText()
    systemDataFrame, specialNames = theScraper.storeTableInDataFrame(soup)
    theScraper.getEntriesWithClients(systemDataFrame)
    print(theScraper.systemsWithClientsDataFrame)
    theScraper.downloadAndStoreExcelFiles(theScraper.systemsWithClientsDataFrame,specialNames)




